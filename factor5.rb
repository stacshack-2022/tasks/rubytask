p = 5
n = ARGV[0].to_i
counter = 0
while n % p == 0 do
    n = n / p 
    counter += 1
end

never_going_to_give_you_up_release = Time.new(1987, 7, 27)
diff_time = Time.now - never_going_to_give_you_up_release
log = "Number of seconds since Never Gonna Give You Up release: %d" % [diff_time.round]

out = "%d, %d, %s" % [p, counter, log]
print(out)